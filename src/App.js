import React, { useEffect } from 'react';
import './App.css'
import { SearchIcon } from './Assets/icons/icons';
// ! import request instead of axios
import { request } from './utils/request';
import TopBar from './components/Topbar/topbar';
import Header from './components/Header/header';
import Navbar from './components/Navbar/navbar';
import Banner from './components/Banner/banner';
import Categories from './components/Category/category';
import Products from './components/Products/products';



const App = () => {
  const getUsers = () => {
    request.get('/users')
      .then(res => {
        console.log('res', res);
      })
      .catch(err => {
        console.log('err', err);
      });
  };

  useEffect(() => {
    getUsers();
  }, []);
  
  return (
    <div className='App'>
      <TopBar/>
      <Header/>
      <Navbar/>
      <Banner/>
      <Categories title={'Популярные категории'}/>
      <Products title={'Хиты продаж'}/>
      {/* <p>Header</p>
      <p>NavBar</p>
      <p>Banner</p>
      <p>ProductList</p> */}
      {/* <SearchIcon/> */}
    </div>
  );
}
 
export default App;