import React from 'react';
import './topbar.css';
import phoneIcon from '../../Assets/icons/phone-icon.png';
import locationIcon from '../../Assets/icons/location_on.png';
import flagRusIcon from '../../Assets/icons/flagRus.png';

function topbar() {
    return (
        <div className='topbar'>
           <div className="tab tab-container">
               
               <div className="tab-actions">
                   <a href="" className='tab-action_link'>Магазины</a>
                   <a href="" className='tab-action_link'>Оставить отзыв</a>
                   <a href="" className='tab-action_link'>Доставка</a>
               </div>
               
               <div className="tab-contactInfo">
                   <a href="tel:+998 97 778-17-08" className='tab-contactInfo_link'>
                       <img src={phoneIcon} alt="" />
                       +998 97 778-17-08
                   </a>
                   <a href="" className='tab-contactInfo_link'>
                       <img src={locationIcon} alt="" />
                       Ташкент
                   </a>
                   <select name="" id="select">
                       <option value="rus">
                       <img src={flagRusIcon} alt="" />
                       Рус
                       </option>
                       <option value="uzb">
                       <img src={flagRusIcon} alt="" />
                       Uzb
                       </option>
                       <option value="Eng">
                       <img src={flagRusIcon} alt="" />
                       Eng
                       </option>
                   </select>
               </div>
           </div>
        </div>
    );
}

export default topbar;