import React from 'react';
import './navbar.css';


function topCategory() {
    return (
        <div className='navbar'>
            <div className="navbar-container">
               <div className="navbar-links">
               <a href="#" className="navbar-links_item">Акции и скидки</a> 
               <a href="#" className="navbar-links_item">Смартфоны и гаджеты</a> 
               <a href="#" className="navbar-links_item">Телевизоры и аудио</a> 
               <a href="#" className="navbar-links_item">Техника для кухни</a> 
               <a href="#" className="navbar-links_item">Красота и здоровье</a> 
               <a href="#" className="navbar-links_item">Ноутбуки и компьютеры</a> 
               </div>
            </div>
        </div>
    );
}

export default topCategory;