import React from 'react';
import './products.css';
import Product1 from '../../Assets/images/product1.png';
import Product2 from '../../Assets/images/product2.png';
import Start from '../../Assets/images/Star 1.png';
import AddCart from '../../Assets/icons/addCard-icon.png';
import AddFavIcon from '../../Assets/icons/Favourite-icon.png';
import AddCompIcon from '../../Assets/icons/Compare-icon.png';
import FavClickedIcon from '../../Assets/icons/favClicked-icon.png';
import CompClickedIcon from '../../Assets/icons/compClicked-icon.png';


function products({title}) {
    return (
        <div className='productList'>
          <div className="productList-container">
          <h2 className='productTitle'>{title}</h2>  
          <div className="productList-card">
              <div className="card">
                  <div className="card-img">
                  <img src={Product1} alt="" />
                  </div>
                  <div className="card-info">
                  <h4 className='productName'>Samsung Galaxy A41 Red 64 GB</h4>
                  <p className='productPrice'>3 144 000 сум</p>
                  <p className="creditInfo">От 385 000 сум/12 мес</p>
                  <div className="rating">
                      <button className='starBtn'><img src={Start} alt="" /></button>
                      <button className='starBtn'><img src={Start} alt="" /></button>
                      <button className='starBtn'><img src={Start} alt="" /></button>
                      <button className='starBtn'><img src={Start} alt="" /></button>
                      <button className='starBtn'><img src={Start} alt="" /></button>
                  </div>
                  </div>
                  <div className="product-actions">
                      <button className='addCartBtn'><img src={AddCart} alt="" />В корзину</button>
                      <button className='addCompare'><img src={AddCompIcon} alt="" /></button>
                      <button className='addFavourite'><img src={AddFavIcon} alt="" /></button>
                  </div>
              </div>
              
              <div className="card">
                  <div className="card-img">
                  <img src={Product1} alt="" />
                  </div>
                  
                  <div className="card-info">
                  <h4 className='productName'>Samsung Galaxy A41 Red 64 GB</h4>
                  <p className='productPrice'>3 144 000 сум</p>
                  <p className="creditInfo">От 385 000 сум/12 мес</p>
                  <div className="rating">
                      <button className='starBtn'><img src={Start} alt="" /></button>
                      <button className='starBtn'><img src={Start} alt="" /></button>
                      <button className='starBtn'><img src={Start} alt="" /></button>
                      <button className='starBtn'><img src={Start} alt="" /></button>
                      <button className='starBtn'><img src={Start} alt="" /></button>
                  </div>
                  </div>
                  <div className="product-actions">
                      <button className='addCartBtn'><img src={AddCart} alt="" />В корзину</button>
                      <button className='addCompare'><img src={AddCompIcon} alt="" /></button>
                      <button className='addFavourite'><img src={AddFavIcon} alt="" /></button>
                  </div>
              </div>
              
              <div className="card">
                  <div className="card-img">
                  <img src={Product1} alt="" />
                  </div>
                  <div className="card-info">
                  <h4 className='productName'>Samsung Galaxy A41 Red 64 GB</h4>
                  <p className='productPrice'>3 144 000 сум</p>
                  <p className="creditInfo">От 385 000 сум/12 мес</p>
                  <div className="rating">
                      <button className='starBtn'><img src={Start} alt="" /></button>
                      <button className='starBtn'><img src={Start} alt="" /></button>
                      <button className='starBtn'><img src={Start} alt="" /></button>
                      <button className='starBtn'><img src={Start} alt="" /></button>
                      <button className='starBtn'><img src={Start} alt="" /></button>
                  </div>
                  </div>
                  <div className="product-actions">
                      <button className='addCartBtn orderBtn1'>Заказать</button>
                      <button className='addCompare'><img src={AddCompIcon} alt="" /></button>
                      <button className='addFavourite'><img src={FavClickedIcon} alt="" /></button>
                  </div>
              </div>
              
              <div className="card">
                  <div className="card-img">
                  <img src={Product2} alt="" />
                  </div>
                  <div className="card-info">
                  <h4 className='productName'>Samsung Galaxy A41 Red 64 GB</h4>
                  <p className='productPrice'>3 144 000 сум</p>
                  <p className="creditInfo">От 385 000 сум/12 мес</p>
                  <div className="rating">
                      <button className='starBtn'><img src={Start} alt="" /></button>
                      <button className='starBtn'><img src={Start} alt="" /></button>
                      <button className='starBtn'><img src={Start} alt="" /></button>
                      <button className='starBtn'><img src={Start} alt="" /></button>
                      <button className='starBtn'><img src={Start} alt="" /></button>
                  </div>
                  </div>
                  <div className="product-actions">
                      <button className='addCartBtn orderBtn2'>Заказать</button>
                      <button className='addCompare'><img src={CompClickedIcon} alt="" /></button>
                      <button className='addFavourite'><img src={AddFavIcon} alt="" /></button>
                  </div>
              </div>
          </div>
          </div>
        </div>
    );
}

export default products;