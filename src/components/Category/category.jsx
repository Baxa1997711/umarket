import React from 'react';
import './category.css';
import Category1 from '../../Assets/images/category1.png'
import Category2 from '../../Assets/images/category2.png'
import Category3 from '../../Assets/images/category3.png'
import Category4 from '../../Assets/images/category4.png'


function categories({title}) {
    return (
        <div className='category'>
            <div className="category-container">
                <h2>{title}</h2>
                <div className="product-list">
                <div className="top-products">
                    <img src={Category1} alt="" />
                    <h4>Смартфоны</h4>
                </div>
                <div className="top-products">
                    <img src={Category2} alt="" />
                    <h4>Мониторы</h4>
                </div>
                <div className="top-products">
                    <img src={Category3} alt="" />
                    <h4>Компьютеры</h4>
                </div>
                <div className="top-products">
                    <img src={Category4} alt="" />
                    <h4>Аксессуары</h4>
                </div>
                </div>
            </div>
        </div>
    );
}

export default categories;