import React from 'react';
import './header.css';
import LogoIcon from '../../Assets/icons/Logo.png';
import { SearchIcon } from '../../Assets/icons/icons';
import PhotoSearch from '../../Assets/icons/PhotoSearch.png';
import KorzinaIcon from '../../Assets/icons/Korzina-icon.png';
import FavoriteIcon from '../../Assets/icons/Favourite-icon.png';
import CompareIcon from '../../Assets/icons/Compare-icon.png';
import LoginIcon from '../../Assets/icons/Login-icon.png';




function header() {
    return (
        <div className='header'>
            <div className="header-container">
                <div className="logo">
                    <a href="#">
                        <img src={LogoIcon} alt="" />
                    </a>
                </div>
                
                <div className="search">
                    <div className="search-tips">
                        <div className="search-input_items">
                        <button className='search-btn'><SearchIcon/></button>
                        <input type="text" 
                        placeholder='Поиск по товарам'
                        />
                        </div>
                        <button className='photo-search'><img src={PhotoSearch} alt="" /></button>
                    </div>
                </div>
                
                <div className="carts">
                    <div className="carts-item">
                        <img src={KorzinaIcon} alt="" />
                        <a href='#'>Корзина</a>
                    </div>
                    <div className="carts-item">
                        <img src={FavoriteIcon} alt="" />
                        <a href='#'>Избранные</a>
                    </div>
                    <div className="carts-item">
                        <img src={CompareIcon} alt="" />
                        <a href='#'>Сравнение</a>
                    </div>
                    <div className="carts-item">
                        <img src={LoginIcon} alt="" />
                        <a href='#'>Войти</a>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default header;