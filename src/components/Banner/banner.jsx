import React from 'react';
import Banner from '../../Assets/images/image 2.png';
import './banner.css';

function banner() {
    return (
        <div className='banner'>
            <div className="banner-img">
                <img src={Banner} alt="" />
            </div>
        </div>
    );
}

export default banner;